#!/usr/bin/env bash

IP_ADDR="$1"

command -v gst-launch-1.0 || sudo pacman -S gstreamer
command -v wf-recorder || sudo pacman -S wf-recorder

main() {
    sudo modprobe -r v4l2loopback
    sudo modprobe v4l2loopback video_nr=3 card_label=virtcam exclusive_caps=1 max_buffers=2
    _stream_screen & _record_screen && fg
    wait
}


function _record_screen {
    wf-recorder --muxer=v4l2 --codec=rawvideo --file=/dev/video3 -x yuv420p
}

function _stream_screen {
    sleep 5
    gst-launch-1.0 v4l2src device=/dev/video3 \
                 ! video/x-raw,framerate=30/1 \
                 ! queue ! jpegenc ! rtpjpegpay \
                 ! udpsink host="$IP_ADDR" \
                           port=50001

}

main
