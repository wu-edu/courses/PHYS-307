#+title: TOPICS

Topics will be written in a combination of ~bash~, ~Rust~, ~python~, and (sorry Anas), ~javascript~

* Roster
|---+------------+--------+---------------------|
| n | Last       | First  | Email               |
|---+------------+--------+---------------------|
| 1 | Butler     | Will   | hutwj-26@rhodes.edu |
| 2 | Matar      | Anas   | matak-25@rhodes.edu |
| 3 | Nguyen     | Phuc   | nguph-24@rhodes.edu |
| 4 | Scherz     | Jasper | schjw-26@rhodes.edu |
| 5 | Tang       | Kenny  | tanke-25@rhodes.edu |
| 6 | Wong Zheng | Kairos | wonzh-24@rhodes.edu |
|---+------------+--------+---------------------|
* Throughout Semester
- Dotfiles
- High performace/cloud computing
- git Unix and regex
- Netlify and static site generation
- AI/ML (pytorch/tensorflow)
** Netizenship
- Licenses
- Free and Open Source Software
- Tech and Society
* Physics Topics
- Realistic projectile motion
- Potentials/Fields/Materials (E&M)
- Complex systems/emergent behavior
- Statistical Mechanics
- Navier-stokes
- Fourier transforms/digital signal processing
- Quantum Mechanics
- High Energy Physics
- Reaction diffusion simulations
- Turing patterns
* Selected topics
|---+------------+--------+--------------------------------------|
| n | Last       | First  | Topic                                |
|---+------------+--------+--------------------------------------|
| 1 | Butler     | Will   | Stat mech                            |
|   |            |        | Netlify and static site generation   |
|---+------------+--------+--------------------------------------|
| 2 | Matar      | Anas   | Quantum Mechanics                    |
|   |            |        | HEPEC                                |
|---+------------+--------+--------------------------------------|
| 3 | Phuc       | Albert | Oscillatory Motion and Chaos         |
|   |            |        | The Solar System                     |
|---+------------+--------+--------------------------------------|
| 4 | Scherz     | Jasper | Potentials/Fields                    |
|   |            |        | Fourier Transforms/signal Processing |
|---+------------+--------+--------------------------------------|
| 5 | Tang       | Kenny  | Relativistic Projectile Motion       |
|   |            |        | Navier-Stokes                        |
|---+------------+--------+--------------------------------------|
| 6 | Wong Zheng | Kairos | Complex Systems/Emergent Behavior    |
|   |            |        | Turing Patterns                      |
|---+------------+--------+--------------------------------------|
|   | Wu         | Sean   | Statistical Mechanics                |
|   |            |        | Dotfiles                             |
|---+------------+--------+--------------------------------------|
