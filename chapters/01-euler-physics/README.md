# Euler Physics

The Euler Method will provide our first experience with numerical techniques for solving differential equations.  We will begin by deriving it for exponential decay and then will apply it to variety of physical scenarios.

## The Numerical Approximation of an Exponential Decay

Consider a function of time, $N(t)$, describing some population which is given by:

$$\frac{dN(t)}{dt} = -\alpha N(t)$$

Solving this equation analytically yields the equation for exponential decay:

$$N(t) = N_0 e^{-\alpha t}$$

Where $N_0$ is the population at some initial time. It can be approximated via Taylor expansion for small, but finite timesteps:


$$N(\Delta t) = N(0) + \frac{dN}{dt} \Delta t + \frac{d^2N}{dt^2} (\Delta t)^2 \ldots$$

Given $\Delta t$ is small, higher order terms are approximately zero reducing our equation to:

$$N(\Delta t) \approx N(0) + \frac{dN}{dt} \Delta t$$

If this approximation is evaluated at some time $t>0$, it becomes:

$$N(t + \Delta t) \approx N(t) + \frac{dN}{dt} \Delta t$$

From here we can plug in the original relationship for $\frac{dN(t)}{dt}$ finally yielding:


$$\boxed{N(t + \Delta t) \approx N(t) - \alpha N(t) \Delta t}$$

## Writing $N(t)$ in Terms of Timesteps

In order to approximate our original function, we calculated the values of $N$ by referencing previous values. Rather than writing $N$ in terms of $t$ and $\Delta t$, we write it in terms of integer steps $t_{i+1}$ and $t_i$. The boxed equation is now:

$$N(t_{i+1}) = N(t_i) - \alpha N(t_i) \Delta t$$

Factoring, we see that:

$$\boxed{N(t_{i+1}) = N(t_i)(1 - \alpha \Delta t)}$$

We $N(t)$ is written in a discrete rather than functional form. Using this information, procede to write code representing it in  `euler_function.py`

## Writing and Testing our Algorithm

Open `euler_function.py` and do the following:

1. Edit the function `next_N` to reflect the algorithm obtained in the previous section.
1. Change the `while loop` so it exits once a cutoff value has been achieved.
1. Change `some_var`, `another_var`, and the arguments of `test_function` so they reference the appropriate input variables.
1. Choose appropriate input parameters.
1. Run `euler_function.py` so that it outputs to the terminal. Is this output consistent with your expectations given the initial function?

## Radiocarbon Dating

Biological organisms breathe Carbon from the atmosphere while they are living. When an organism dies, C14 begins to decay to C12. Since the percentage of C14 in a living organism is known, measuring the amount of C14 remaining in a dead organism determines how long ago it died.

1. If the number of C14 atoms is given by:
$$N(t)=N_0 e^{-\frac{t}{\tau}}$$
and the half-life of C14 is 5700 years, find $\tau$ if by definition the half-life is the amount of time it takes for there to be half of the species remaining.
1. We will be writing a recursive algorithm. A recursive algorithm has two parts. A calculation, and an exit condition. Define an exit condition in terms of the given variables. It will be similar to the cutoff from before.
1. Write your recursive function first to `print` your values to the terminal, then to collect them in an array.
1. The two arguments of `plt.plot` must be arrays. Collect your printed output to two such arrays in order to generate the plot.

## Breeder Reactions

In nuclear physics, decays are not as simple as one species decaying to zero. In a special kind of nuclear reactor called a **Breeder Reactor** as one nuclear isotope decays another is produced.  Such reactors are advantageous as they produce fuel as they expend it.  One such reactor uses Uranium 238 to produce Plutonium and Neptunium. Consider such a reactor with Uranium and Plutonium. Given the relationship between the decays of Uranium and Plutonium:

$$\frac{dN_U}{dt} = - \frac{N_U}{\tau_U}$$
$$\frac{dN_P}{dt} = \frac{N_U}{\tau_U} - \frac{N_P}{dt}$$

Write code using either a `while` loop or recursion to plot both species as a function of time. Tune your input parameters so you see Plutonium produced as Uranium is expended. Good Luck!
