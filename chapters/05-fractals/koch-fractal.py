# import turtle

# def koch_fract(turtle, divis, size):
#     if divis == 0:
#         turtle.forward(size)
#     else:
#         for angle in [60, -120, 60, 0]:
#             koch_fract(turtle, divis - 1, size / 3)
#             turtle.left(angle)

# divis = 10
# size = 2000

# wn = turtle.Screen()
# wn.setup(width=1000, height=500)

# turtle.penup()
# turtle.goto(-500, 150)

# turtle.speed(100)

# turtle.pendown()

# for i in range(0, 3):
#     koch_fract(turtle, divis, size)
#     turtle.left(-120)




# # Draw a Koch snowflake


# # from turtle import *

# def koch(a, order):
#     if order > 0:
#         for t in [60, -120, 60, 0]:
#             forward(a/3)
#             left(t)
#     else:
#         forward(a)

# # Test
# koch(100, 0)
# pensize(3)
# koch(100, 1)
# from turtle import *

# def snowflake(length_side,level):
# 	if level==0:
# 		forward(length_side)

# 	else:
# 		return

# 	length_side=length_side/3.0

# 	snowflake(length_side,level-1)
# 	left(60)
# 	snowflake(length_side,level-1)
# 	right(120)
# 	snowflake(length_side,level-1)
# 	left(60)
# 	snowflake(length_side,level-1)


# 	if __name__=='__main__':

# 		speed(0)
# 		length=300.0

# 		penup()

# 		backward(length/2.0)

# 		pendown()

# 		for i in range(3):
# 			snowflake(length,4)
# 			right(120)

# 		mainloop()
# Python program to print complete Koch Curve.
from turtle import *

# function to create koch snowflake or koch curve
def snowflake(lengthSide, levels):
	if levels == 0:
		forward(lengthSide)
		return
	lengthSide /= 3.0
	snowflake(lengthSide, levels-1)
	left(60)
	snowflake(lengthSide, levels-1)
	right(120)
	snowflake(lengthSide, levels-1)
	left(60)
	snowflake(lengthSide, levels-1)

# main function
if __name__ == "__main__":
	# defining the speed of the turtle
	speed(0)
	length = 300.0

	# Pull the pen up – no drawing when moving.
	# Move the turtle backward by distance, opposite
	# to the direction the turtle is headed.
	# Do not change the turtle’s heading.
	penup()

	backward(length/2.0)

	# Pull the pen down – drawing when moving.
	pendown()
	for i in range(3):
		snowflake(length, 4)
		right(120)

	# To control the closing windows of the turtle
	mainloop()
