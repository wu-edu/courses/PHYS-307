import numpy as np
import matplotlib.pyplot as plt

fig,ax = plt.subplots(2,2)

### Constants ###
B2_pm = 4 * pow(10, -5)
g = 9.81
dt  = .5

### Part A ###
v_0 = 750
theta_n = [ 15.0, 30.0, 45.0, 60.0, 75.0 ]

## Part B ###
theta_0 = 50
v_n = [ 500.0, 750.0, 1000.0, 1250.0, 1500.0 ]

### Position Arrays ###
x_n = [0]
y_n = [0]


def update_position(x_n, y_n, v_x, v_y, B2 ):
    v=np.sqrt(pow(v_x, 2) + pow(v_y, 2))

    v_x=
    v_y=

    # x_n=?
    # y_n=?

    return True if y_n[-1] < 0 else update_position(x_n, y_n, v_x, v_y, B2)

def gen_angle_plot(px, py, B2):
    for angle in theta_n:
        x_n = [0]; y_n = [0]
        update_position(x_n, y_n, v_0 * np.cos(np.deg2rad(angle)), v_0 * np.sin(np.deg2rad(angle)), B2)
        ax[px,py].plot(x_n, y_n)

def gen_velocity_plot(px, py, B2):
    for vel in v_n:
        x_n = [0]; y_n = [0]
        update_position(x_n, y_n, vel * np.cos(np.deg2rad(theta_0)), vel * np.sin(np.deg2rad(theta_0)), B2)
        ax[px,py].plot(x_n, y_n)


gen_angle_plot(0, 0, 0)
gen_velocity_plot(0, 1, 0)
gen_angle_plot(1, 0, B2_pm)
gen_velocity_plot(1, 1, B2_pm)

plt.show()

