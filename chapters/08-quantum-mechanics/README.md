In the 1860\'s, as James Clerk Maxwell was composing his famous
equations, physicists were interested in other unexplained phenomena.
Chief among these was the nature of light. Unlike the continuous color
spectrum created by light passing through a prism, when viewed through a
diffraction grating, discrete bands are observed. This puzzle, among
others, ultimately led to the formulation of Quantum Mechanics.

# Theory of Discrete Lines

Johannes Rhydberg provided a theoretical basis for these observations
with the following equation:
$$\frac{1}{\lambda} = R_H\left(\frac{1}{n^2} - \frac{1}{m^2}\right)$$

This equation accurately described the observed phenomena, but failed to
provide a cause.

# The Bohr Model

By the beginning of the 1900\'s, it was experimentally known that an
atom was composed of a dense positive center, and electrons orbiting it.
Classical electromagnetic theory could not be reconciled with these
experiments. According to Maxwell\'s equations, when an electron
accelerates, it emits electromagnetic radiation. In the case of an
orbital model of the atom, this means that the electron would lose
energy causing it to spiral in towards the center of the atom.

Niels Bohr\'s solution was to take the classical interpretation and
combine it with new theories that involved quantization. Quantization
here meaning discrete rather than continuous energies. Bohr\'s theory
has the following features:

1.  Stationary states with a definite total energy
2.  Discrete emission and absorption of energy
    ($\Delta E = (E_2 - E_1)= hf$)
3.  Dynamics described by Classical Mechanics (inclusive of
    Electrodynamics)
4.  Quantized angular momentum

The derivation of the Bohr model is as follows. Angular momentum is
given by:

$$\vec{L} = \vec{r}\times\vec{p} \qquad L=mvr=n\hbar$$

Solving for $v$:

$$v=\frac{n\hbar}{mr}$$

From Coulomb\'s Law:

$$F_{q_1q_2}=\frac{1}{4\pi\epsilon_0} \frac{q_1q_2}{r^2}$$

and the centripetal force: $$F_C=\frac{mv^2}{r}$$

and with $q_1 = q_2 = q$:
$$\frac{mv^2}{r}=\frac{1}{4\pi\epsilon_0} \frac{q_1q_2}{r^2}$$

or

$$mv^2=\frac{1}{4\pi\epsilon_0} \frac{q_1q_2}{r}$$

so

$$m \frac{n^2\hbar^2}{m^2r^2}=\frac{1}{4\pi\epsilon_0} \frac{q_1q_2}{r}$$

solving for r yields:

$$r_n = 4\pi\epsilon_0\frac{n^2\hbar^2}{m^2q^2}n^2$$

If $a_0 =  4\pi\epsilon_0n^2\hbar^2 \textit{/}m^2q^2$, then

$$\boxed{r_n = a_0 n^2}$$

$a_0$ is what is known as the Bohr radius. The velocity and energy can
likewise be shown to be:

$$v_n=\frac{1}{4\pi\epsilon_0}\frac{q^2}{\hbar}\frac{1}{n} \qquad E_n = - \frac{1}{32\pi^2 \epsilon_0^2}\frac{m_e q^2}{\hbar^2}\frac{1}{n^2}$$

Defining:

$$E_0 = - \frac{1}{32\pi^2\epsilon_0^2}\frac{m_e q^2}{\hbar^2}$$

Then:

$$E_n = -E_0 \frac{1}{n^2}$$

$E_0=-13.6 eV$ which isd known as the Ground State Energy.

The Bohr atom was conceived as a description of the atom, yet it
connects to the Rhydberg formula exactly as:

$$R_H = \frac{E_0}{hc}$$

While successful in explaining simple atomic spectroscopy, the Bohr
model failed to explain:

1.  The nature of atoms with more than one electron
2.  The intensity of spectral lines
3.  Binding of atoms to form molecules

For that, we would need the Schrodinger Equation

# The Schrodinger Equation

The Schrodinger Equation can be easily solved analytically in three
cases: the infinite potential well, the harmonic oscillator, and the
hydrogen atom. It is the latter which generalizes the Bohr model to
include the entire periodic table (NBD), but for the sake of simplicity
for our studies we will consider the infinite potential well.
