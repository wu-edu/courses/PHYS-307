# Simple Harmonic Motion

Applying Newton\'s Second Law to a pendulum, it can be shown that:

$$F_{\theta} = -g \sin \theta$$

With a pendulum of length $l$, the angle as a function of time
$\theta(t)$ can be found from

$$\frac{d^2 \theta}{dt^2} = - \Omega^2 \sin \theta$$

with $\Omega = \sqrt{ g/l} $

In a typical introductory course, we would Taylor expand $\sin \theta$,
which provides a solution of the form:

$$\theta(t) = \sin (\omega t + \phi)$$

Where $ \phi $ is a phase factor determined by the initial conditions in
angle and angular velocity. To begin the exploration of chaotic motion,
we will start with this simple equation and apply the Euler Method. To
do so, write the original second order differential equation in terms of
two first order differential equations:

$$\frac{d\omega^2}{dt} = - \frac{g}{l} \theta \qquad \frac{d\theta}{dt} = \omega$$

Now, discretize it as:

$$\omega_{i+1} = \omega_i - \Omega \Delta t \qquad \theta_{i+1} = \theta_i + \omega_{i} \Delta t$$

## Example 1

Copy `example_1.py` into the `problems` directory. This applies the
above equation. Does it work according to your expectations? Why or why
not?

## Exploring the Euler Solution to SHM

The plot we obtained seems to violate conservation of energy. As time
goes on, with no forces other than tension and gravity, the pendulum\'s
amplitudes increase. Let\'s compare conservation of energy analytically
to this.

$$E = \frac{1}{2} m l^2 \omega ^2 + mgl(1 - \cos \theta )$$

For small $\theta$ this becomes:

$$E = \frac{1}{2} m l^2 ( \omega ^2 + \frac{g}{l} \theta ^2 )$$

Substituting the above $\omega(t)$ and $\theta(t)$ to apply the Euler
Method:

$$E_{i+1} = E_i +\frac{1}{2} m l^2 ( \omega_{i} ^ {2} + \frac{g}{l} \theta ^ 2)(\Delta t) ^ 2$$

This clearly violates conservation of energy, explaining why the plot
diverges.

## Problem 1

Modify Example 1 so that:

$$\omega_{i+1} = \omega_i - \Omega \Delta t \qquad \theta_{i+1} = \theta_i + \omega_{i} \Delta t$$

to see if this fixes the issue. Commit these changes and push them to
GitLab.

# Complex Harmonic Motion

There are three additional considerations to make the pendulum more
realistic: dampening, driving, and a larger angle. We will explore each
one by one before we consider the chaotic case of all three
simultaneously.

## Damped Harmonic Motion

In the case of damped harmonic motion, we add an additional term to our
simple harmonic motion:
$$\frac{d^2 \theta}{dt^2} = -\frac{g}{l}\theta - q \frac{d\theta}{dt}$$

where $q$ describes the dampening. This equation can be solved
analytically: so that:

$$\theta(t) = \theta_0 e^{-qt/2} \sin (\sqrt{\Omega^2 -q^2/4} t + \phi)$$

## Problem 2

Copy `example_2.py` to `problem_2.py` in the `problems` directory. Show
several examples of under, over, and critically damped. Take screenshots
showing the plots and values.

## Driven Harmonic Motion

Driven harmonic motion is a further extension of damped harmonic motion.
Rather than a homogeneous equation, driven harmonic motion contains a
periodic driving term resulting in an equation such as:

$$\frac{d^2 \theta}{dt^2} = -\frac{g}{l}\theta - q \frac{d\theta}{dt} + F_D \sin (\Omega_D t) $$

This equation is analytically soluble with a steady state solution of
the form:

$$\theta(t) = \theta_0 e^{-qt/2} \sin (\sqrt{\Omega^2 -q^2/4} t + \phi)$$

And

$$\theta_0 = \frac{F_D}{\sqrt{(\Omega^2 - \Omega^2_D)^2 + (q\Omega_D)^2}}$$

## Problem 3

Copy `example_2.py` to `problem_3.py` in the `problems` directory. By
the superposition of solutions, show that after a long time, the damped
driven oscillator approaches the function describing the driving term.

## Large Angle Oscillations

So far, we have taken the small angle approximation
$\sin \theta \approx \theta$, but this isn\'t true in general. The
general harmonic oscillator, takes the form

$$\frac{d^2\theta}{dt^2} = - \frac{g}{l} \sin \theta$$

## Problem 4

Write a program describing the large angle harmonic oscillator. Show
that its period of oscillation is dependent on the starting angle.
