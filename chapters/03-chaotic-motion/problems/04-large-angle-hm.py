import numpy as np
import matplotlib.pyplot as plt

f,ax = plt.subplots(1)

### Constants ###
g       = 9.81
l       = 2.5
dt  = 0.1
t_f = 10

### Arrays ###
t_n     = [0]
angle_array = [np.pi/2, np.pi/3, np.pi/4, np.pi/5, np.pi/10]
omega_n = [0]
theta_n=[0]


def update_angle(t_n, theta_n, omega_n):

    t_n.append(t_n[-1] + dt )
    omega_n.append(omega_n[-1] - g / l * np.sin( theta_n[-1]) * dt)
    theta_n.append(theta_n[-1] + omega_n[-1] * dt)

    return True if t_n[-1] > t_f else update_angle(t_n, theta_n, omega_n)


def gen_angle_plot():
    for angle in angle_array:
        print(angle)
        t_n = [0]; theta_n = [angle]; omega_n=[0]
        update_angle(t_n, theta_n, omega_n)
        ax.plot(t_n, theta_n)


gen_angle_plot()

plt.title('Large Angle Harmonic Motion', fontsize=18)
plt.xlabel('Time (s)', fontsize=12)
plt.ylabel('Angle (Rad)', fontsize=12)

#ax.set_xlim(left=0)
ax.set_xlim([0,10])

plt.show()
