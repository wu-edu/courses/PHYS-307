import sys
import numpy as np
import matplotlib.pyplot as plt
from random import randrange

fig, axs = plt.subplots(1)

sys.setrecursionlimit(10**9)

n_steps = 1000
dir_arr = [ (1,0), (-1,0), (0,1), (0,-1) ]
pos_arr = [(0,0)]

def surrounded(x,y):
    for dir in dir_arr:
        (dx,dy) = dir
        if (x+dx,y+dy) not in pos_arr:
            return False

    print("Surrounded!")
    return True

def walk(steps_rem):
    (x,y) = pos_arr[-1]
    if surrounded(x,y):
        return
    else:
        (dx,dy) = dir_arr[randrange(4)]
        new_pos = (x+dx,y+dy)
        if new_pos in pos_arr:
            walk(steps_rem)
        else:
            pos_arr.append(new_pos)
            return True if steps_rem == 0 else walk(steps_rem-1)

walk(n_steps-2)

print(pos_arr)
print("Number of completed steps: ", len(pos_arr))

plt.grid(color='grey', linestyle='--', linewidth=.5)
plt.axhline(0, color='black', linewidth=.5)
plt.axvline(0, color='black', linewidth=.5)
plt.plot(*zip(*pos_arr))

plt.show()
