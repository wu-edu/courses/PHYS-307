import numpy as np
import random, sys
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button

fig, axs = plt.subplots(2)

sys.setrecursionlimit(100000)

total_steps = 100

step_arr = range(0, total_steps)

position_arr = [ 0 ]
position_freq = [ 0 ]

def next_step_direction():
    if random.random() < 0.5:
        return 1
    else:
        return -1


def walk(remaining_steps):
    remaining_steps-=1
    position_arr.append(position_arr[-1] + next_step_direction())

    return True if remaining_steps ==  1 else walk(remaining_steps)

walk(total_steps)

axs[0].scatter(step_arr, position_arr, s=1.5)
axs[0].set_xlabel('Step Number', fontsize=12)
axs[0].set_ylabel('Position', fontsize=12)

for run in range(0,1000):
    position_arr = [ 0 ]
    walk(total_steps)
    position_freq.append(position_arr[-1])

axs[1].hist(position_freq, 100)
axs[1].set_xlabel('Steps', fontsize=12)
axs[1].set_ylabel('Frequency', fontsize=12)
plt.show()
