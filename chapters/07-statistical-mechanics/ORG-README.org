#+TITLE: Statistical Mechanics

Statistical mechanics is the study of the emergent behavior of uncountable large systems of particles.  Unlike our discussion of random motion, here systems are self interacting.  That is the state of any given particle is dependent on the particles adjacent to it.  We will see that by considering averages of these micro systems, the manifest behavior of the macro system can be determined.

* The Ising Model
Consider a system of adjacent particles each with a spin.  Either up or down.  Each particle's spin can be represented as $s_i=\pm1$. The energy of the system is given by:

$$ E = - J \sum_{\langle ij \rangle} s_i s_j $$

Where $F$ is a positive value known as the exchange constant, and $\langle ij \rangle$ are the set of all adjacent spins. Energy is lowest when the two spins are aligned.

These spins will align based on external factors such as temperature or electric field.  For a system in equilibrium with a heat bath, the probability of finding the system in any particular state is proportional to the Boltzmann factor:

$$P_\alpha \sim e^{-E_\alpha/k_B T}$$

Where $E_\alpha$ is the eneregy of state $\alpha$ (not to be confused with $i$) and $P_\alpha$ is the probability of finding the system in a state $\alpha$.  If we are describing a magnet, the /measured/ magnetization of the system is given by:

$$M=\sum_{\alpha}M_{\alpha}P_{\alpha}$$

where $M_{\alpha} = \sum s_j$ for some microstate $\alpha$

* Mean Field Theory
Returning to the previous magnetization equation and recognizing all spins must have the same average properties, the total magnetization at temperature $T$ for a system of $N$ spins will then be


$$M=\sum_{\langle s_i \rangle} = N \langle s_i \rangle$$

where in the last term we can use any value of $i$ that is convenient, since we have argued they are all equivalent. Thus, if we can calculate $\langle s_i \rangle$ we immediately have $M$ as well. Since finding this for a system of $N$ particles, we consider an approximate alternative known as /mean field theory/ in our case of magnetization, this can be expressed as:

$$ E = - J \sum_{\langle ij \rangle} s_i s_j - \mu H \sum_i s_i$$

where $H$ is the external magnetic field and $\mu$ is the magnetic moment associated with each spin. The system can be in one of two probabilities $P_{\pm}$ given by:

$$P_+ = C e^{+\mu H/k_B T}$$
$$P_- = C e^{-\mu H/k_B T}$$

Where the coefficient $C$ can be determined by requiring these two probabilities add to 1.  This gives

$$C = \frac{1}{P_+ = C e^{+\mu H/k_B T} + P_- = C e^{-\mu H/k_B T}}$$

The thermal average of $s_i$ can be calculating by summing the probabilities that each spin is in either state:

$$\langle s_i \rangle = \sum_{s_i=\pm1}s_i P_\pm = P_+ - P_- = \tanh(\mu H / k_B T)$$

We can use this to obtain an approximate solution, with the assumption that it is each spin, $s_i$, interacting with its neighboring spins which creates the first term on the RHS of our energy equation. this is equivalent to an effective field, $H_{eff}$. The energy function is rewritten as:

$$ E = - J \left( \sum_{\langle ij \rangle} s_j \right) s_i - \mu H \sum_i s_i $$

which shows that the term involving $J$ (which describes the interaction of $s_i$ with its neighbors) has the form of a magnetic field with $\mu H_{eff}=J\sum s_j$. $H_{eff}$ can be replaced by their thermal averages.  Since all of the spins have the same average alignment, their thermal average values will all be the same.  Denote this  by $\langle s \rangle$ (since we can drop the subscripts) and assuming that the "true" externally applied field is $H=0$ we have:

$$H_{eff} = \frac{J}{\mu}\sum \langle s \rangle = \frac{z J}{\mu} \langle s \rangle $$

Where $z$ is the number of nearest neighbors.  Combining this with the $\langle s_i \rangle = \tanh(\mu H / k_B T)$, we have:

$$\langle s \rangle = \tanh(zJ\langle s \rangle / k_B T)$$

There are potentially two non-zero solutions with $\langle s \rangle = \pm s_0$.  Since these solutions have a nonzero value of $\langle s \rangle$ these correspond to ferromagnetic phases pointing either up or down.
* Monte Carlo Method
We now want to numerically study the properties of our spins/magnetization. Consider that each pair of spins has an energy that is either positive or negative, and let us define $E_{flip}$, the energy required to flip one of the pair. $E_{flip}$ will be calculated from the very first equation of the chapter. If $E_{flip}$ is negative (i.e. energy is lower by reversing the spin), the spin is flipped and the system moves into a different microstate.  If $E_{flip}$  is positive (so that the energy of the system would be increase), a decision must be made.  A random number that is distributed uniformly in the range between 0 and 1 is generated, and compared to the Boltzmann factor, exp($E_{flip}/k_BT$).  If this Boltzmann factor is larger than the random number, the spin is flipped, otherwise the spin is left undisturbed.  This is an example of the Monte Carlo Method.

A Monte Carlo spin flip connects two microstates; let us call their energies $E_1$ and $E_2$

If the system is in state 1, the flipping rules specify the probability for flipping the selected spin during a given time step, hence the rate of transitions from state 1 to state 2. We will call this rate $W (1 \rightarrow 2)$ and since by assumption $E_1 > E_2$, the rules tell us that $W (1 \rightarrow 2) = 1$. Likewise $W (2 \rightarrow 1) = \text{exp}[-(E_1 - E_2) / k_B T)]$ since $E_{flip} = E_1 - E_2 > 0$ When the system is in thermal equilibrium, the probability of finding it in any particular state will, on average, be independent of time, so we expect the number of transitions from state 1 to state 2 must be equal to the number of transitions in the reverse direction.  The number of transitions of a particular kind is proportional to the product of the transition rate $W$ and the probability of the system being found in the appropriate initial state. Equation the number of transitions $1 \rightarrow 2$ and $2 \rightarrow 1$ then gives

$$P_1 W(1 \rightarrow 2) = P_2 W(2 \rightarrow 1)$$

Applying the previous transitions rates,

$$\frac{P_1}{P_2} = \text{exp}[-(E_1 - E_2)/k_B T]$$
** Problem 1
Write a program which applies the Monte Carlo Method in 1D with 100 spins. Begin with each spin randomized as $\pm1$. Then:

1. Run for 100 steps for incremented beginning at $T=0$ and increased by 10 K
2. Plot $\langle s \rangle$ vs. $T$

Since all spins musts interact with adjacent spins, be sure to appropriately treat the first and last spin.

* The Ising Model and Second-Order Phase Transitions
