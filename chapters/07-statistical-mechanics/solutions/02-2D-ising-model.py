import sys, math, random
import numpy as np
import matplotlib.pyplot as plt

fig, axs = plt.subplots(1)

sys.setrecursionlimit(10**9)

## Constants ##
k_B, J = [ 1, 1 ]

## Temperature Variables ##
T_min, T_max = [ 0, 10 ]
n_T = 100

T_scale = T_max / n_T

## Initialize Temperature Array ##
T_arr = []
for T in range(0, n_T + 1):
    T_arr.append(T * T_max / n_T)

## Iterations and Dimensions
N, L = [ 10, 10 ]

## Magnetic Field Variables ##
mu, H = [ 1, 100 ]

## Spin Arrays ##
avg_spin = []
avg_magnetization = []

## Initialize Spin Arrays and Matrices##
spin_avg_arr = np.empty(L)
spin_matrix = []

def rand_spin():
    return -1 if random.random() < 0.5 else 1

def new_spin_matrix():
    spin_mtx=[]
    for i in range(L):
        spin_list=[]
        for j in range(L):
            spin_list.append(rand_spin())
        spin_mtx.append(spin_list)
    return spin_mtx

def update_spin(s_i, s_j):
    def flip_spin(s_i):
        return s_i * - 1

    E_flip = -J * s_i * s_j - mu * H * s_i

    s_i = s_i * -1
    return s_i * -1 if random.random() >= np.exp( - E_flip / k_B * T) else s_i

def parse_spin_matrix(n,m):

    def get_partners(p_1):
        p_0 = L - 1 if p_1 == 0 else p_1 - 1
        p_2 = 0 if p_1 == L - 1 else p_1  + 1
        return [p_0, p_2]

    X, Y = np.random.randint(0, L, size=2)

    x_n, y_n = get_partners(X), get_partners(Y)

    for x in x_n:
        spin_matrix[X][Y] = update_spin(spin_matrix[X][Y], spin_matrix[x][Y])

    for y in y_n:
        spin_matrix[X][Y] = update_spin(spin_matrix[X][Y], spin_matrix[X][y])

    if n == 0 and m==0:
        return True
    elif n == 0:
        n = L; m -= 1

    parse_spin_matrix(n-1,m)

for T in T_arr:
    spin_matrix=new_spin_matrix()
    parse_spin_matrix(L-1,L-1)
    avg_spin.append(np.mean(spin_matrix))
    print(avg_spin[-1])
    avg_magnetization.append(np.sum(spin_matrix)/L**2)

#spin_matrix=np.full((L,L),rand_spin())

spin_matrix=new_spin_matrix()

# spin_matrix = np.random.randint(-1,2, (L,L))
# print(spin_matrix)
# def new_spin_matrix():
#     spin_matrix = np.empty([L,L])
#     spin_matrix = np.full()

# new_spin_matrix()
#print(spin_matrix)
#print(spin_matrix)
#print(np.mean(spin_matrix))

fig.suptitle('Average spin vs. Temperature', fontsize=18)
plt.plot(T_arr, avg_spin)

plt.xlabel('Temperature', fontsize=12)
plt.ylabel('Average Spin', fontsize=12)

plt.show()

# fig.suptitle('Magnetization vs. Temperature', fontsize=18)
# plt.plot(T_arr, avg_magnetization)

# plt.xlabel('Temperature', fontsize=12)
# plt.ylabel('Magnetization', fontsize=12)

# plt.show()
