import numpy as np
import matplotlib.pyplot as plt
import math
import random

fig, axs = plt.subplots(1)

## Constants ##
#k_B=1.38*10**-12
k_B=1
J=1

## Temperature Variables ##
T_0=0
T_inc=2
T_max=8
T_scale=10.
N=100

temp_arr=[T / T_scale for T in range(T_0, T_max * int(T_scale) + T_inc , T_inc)]

## Magnetic Field Variables ##
mu = 1
H = 10


## Array Parameters ##
ising_arr=[]
s_avg=[]
L=10

def new_ising_arr():
    del ising_arr[:]
    for _ in range(L):
        ising_arr.append(-1 if random.random() < 0.5 else 1)

def update_spin(s_i, s_j):
    E_flip = -J * s_i * s_j - mu * H * s_i; s_i = s_i * - 1
    return s_i * -1 if random.random() >= np.exp( - E_flip / k_B * T) else s_i

def ising_walk():
    for j in range(L):
        i = L - 1 if j == 0 else j-1
        k = 0 if j == L - 1 else j+1
        ising_arr[j] = update_spin(ising_arr[j], ising_arr[i])
        ising_arr[j] = update_spin(ising_arr[j], ising_arr[k])

for T in temp_arr:
    new_ising_arr()
    for _ in range(N):
        ising_walk()
    s_avg.append(np.average(ising_arr))

plt.plot(temp_arr, s_avg)
# fig.suptitle('Poincare Section', fontsize=18)

plt.xlabel('Temperature', fontsize=12)
plt.ylabel('Average Spin', fontsize=12)

plt.show()
