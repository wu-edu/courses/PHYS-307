" Source plugins
	so $HOME/.config/nvim/plugin.vim

" General Settings
	set autochdir
	"set scrolloff=5
	set clipboard+=unnamedplus
	set encoding=utf-8
	set hidden
	set mouse=a
	set nocompatible
"	set number relativenumber
	set showmatch
	set splitbelow splitright
	set nowrapscan
    "set iskeyword=@,A-Z

" Tabs and indentation
	set autoindent
	set copyindent
	set shiftround
	set shiftwidth=4
	set smartindent
	set smarttab
	set tabstop=4
	set shell=/bin/bash

" Text formating
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
	autocmd BufWritePre * %s/\s\+$//e
	set nowrap
	set linebreak
	filetype plugin indent on

" Syntax
	syntax on
	syntax enable

" Bottom UI
	set noshowmode
	set noruler
	set noshowcmd
	set laststatus=0
	set cmdheight=1
	set wildmenu

" Disable backing up/vim version control
	set nobackup
	set nowb
	set noswapfile

" Reuse old undos
	try
		set undodir=~/.cache/vim_runtime/temp_dirs/undodir
		set undofile
	catch
	endtry

" Set to auto read when a file is changed from the outside
	set autoread
	autocmd FocusGained,BufEnter * checktime

" Searching (Disable highlight search after enter is pressed)
	set ignorecase
	set incsearch
	set smartcase
	set showmatch
	set nohls

" Remapping some vim defaults
	nnoremap Y y$
	nnoremap j gj

" Set leader as <Space> and set WhichKey
	" set notimeout
	let g:mapleader = " "
	let g:maplocalleader = " "

	nnoremap <silent> Q <Nop>
	nnoremap <silent> q/ <Nop>
	nnoremap <silent> q: <Nop>

" Delete with ctrl-(<BS>|<Del>)
    set backspace=indent,eol,start
    inoremap <c-del> <c-o>dw
" Buffer Nav
	nnoremap <silent> <leader>bb :Buffers <cr>
	nnoremap <silent> <c-b> :buffers <cr>
 	nnoremap <silent> <c-n> :bnext  <cr>
 	nnoremap <silent> <c-p> :PreviousBuffer  <cr>

 	nnoremap <silent> <Leader>bn :bnext <cr>
 	nnoremap <silent> <Leader>bp :bprevious <cr>
 	nnoremap <silent> <Leader>bd :bdelete <cr>

	set timeoutlen=500

" Save to ctrl-s
	nnoremap <silent> <c-s> :w <cr>

" make completion navigation in command line use arrow keys
	cnoremap <Up> 	 <c-p>
	cnoremap <Down>  <c-n>

" Visual mode search
	vnoremap <silent> / :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
	vnoremap <silent> ? :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

" vim-repeat I don't know what this does though?
"	meant to use . with other tpope stuff
	silent! call repeat#set("g>vim-repeat", v:count)

" theme
	"colorscheme Tomorrow-Night-Bright
	"hi Normal guibg=NONE ctermbg=NONE
"    let g:solarized_termtrans=1
"	colorscheme solarized
"
	let g:BASH_InsertFileHeader = 0

" airline
	let g:airline_section_a = ''
	let g:airline_section_c = '%t'
	let g:airline_section_x = '%y' "[1:-1]
	let g:airline_theme = 'minimalist'
	let g:airline#extensions#whitespace#enabled = 0

" YCM
	"let g:ycm_server_python_interpreter = 'python3'
